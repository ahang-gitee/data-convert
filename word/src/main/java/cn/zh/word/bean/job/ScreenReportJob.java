package cn.zh.word.bean.job;

import cn.zh.word.bean.service.ScreenDailyReportServiceImpl;
import cn.zh.word.bean.utils.DateUtil;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;

/**
 * @author zh
 */
@Component
public class ScreenReportJob {

    @Resource
    private ScreenDailyReportServiceImpl screenDailyReportService;


    @Scheduled()
    public void generateReport() throws IOException {
        LocalDate date = LocalDate.now().minusDays(1);
        File file = new File("classpath:report" + File.separator + DateUtil.yearDateConvert(date) + "需求响应日报");
        screenDailyReportService.generateScreenReport(date, false, new FileOutputStream(file));
    }

}
