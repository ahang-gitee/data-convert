package cn.zh.word.bean.mapper;

import cn.zh.word.bean.TradeLoadCurveEO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * @author zh
 */
@Mapper
public interface TradeLoadCurveMapper extends BaseMapper<TradeLoadCurveEO> {

    TradeLoadCurveEO findByDateAndType(@Param("date") LocalDate date,
                                       @Param("type") Integer type);

    List<TradeLoadCurveEO> findByDate(LocalDate date);
}
