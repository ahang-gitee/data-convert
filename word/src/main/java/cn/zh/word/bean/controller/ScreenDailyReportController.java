package cn.zh.word.bean.controller;

import cn.hutool.core.io.IoUtil;
import cn.zh.word.bean.service.ScreenDailyReportService;
import cn.zh.word.bean.utils.DateUtil;
import cn.zh.word.utils.GenerateWordUtils;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.util.PoitlIOUtils;
import jdk.jpackage.internal.IOUtils;
import org.apache.poi.POIDocument;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;

/**
 * @author zh
 */
@RestController
@RequestMapping("/screen-report")
public class ScreenDailyReportController {

    @Resource
    private ScreenDailyReportService reportService;

    @PostMapping("/create")
    public void create(@RequestParam LocalDate date, HttpServletResponse response) throws IOException {
        reportService.create(date, response);
    }

    @PostMapping("/update")
    public void update(@RequestParam LocalDate date) throws IOException {
        reportService.update(date);
    }

    @PostMapping("/download")
    public void download(@RequestParam LocalDate date, HttpServletResponse response) throws IOException {
        File file = ResourceUtils.getFile("classpath:report" + File.separator +
                DateUtil.yearDateConvert(date) + "需求响应日报");
        FileInputStream fis = new FileInputStream(file);
        IoUtil.copy(fis, response.getOutputStream());
        PoitlIOUtils.closeQuietlyMulti(fis, response.getOutputStream());
    }
}
