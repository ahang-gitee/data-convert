package cn.zh.word.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author zh
 */
@Data
@TableName("ele_load_info")
public class EleLoadInfoEO {

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 当天日期
     */
    private LocalDate date;

    /**
     * 最大用电负荷
     */
    private BigDecimal maxEleLoad;

    /**
     * 最大用电负荷产生时间
     */
    private String maxEleLoadTime;

    /**
     * 相对前一日增加或减少负荷
     */
    private BigDecimal relativeLoad;

    /**
     * 相对前一日增加或减少负荷所占百分比
     */
    private BigDecimal relativeLoadRate;

    /**
     * 当天最大降温负荷
     */
    private BigDecimal maxCoolingLoad;

    /**
     * 当天最大降温负荷环比变化
     */
    private BigDecimal maxCoolingRelativeLoad;

    /**
     * 最大降温负荷占最大用电负荷比率
     */
    private BigDecimal coolingRate;

    /**
     * 第二产业最大降温负荷
     */
    private BigDecimal secondMaxCoolingLoad;

    /**
     * 第二产业最大降温负荷环比变化
     */
    private BigDecimal secondMaxCoolingRelativeLoad;

    /**
     * 第三产业最大降温负荷
     */
    private BigDecimal thirdMaxCoolingLoad;

    /**
     * 第三产业最大降温负荷环比变化
     */
    private BigDecimal thirdMaxCoolingRelativeLoad;

    /**
     * 居民最大降温负荷
     */
    private BigDecimal residentMaxCoolingLoad;

    /**
     * 居民最大降温负荷环比变化
     */
    private BigDecimal residentMaxCoolingRelativeLoad;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;
}
