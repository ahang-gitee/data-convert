package cn.zh.word.bean.utils;

import java.time.LocalDate;

/**
 * @author zh
 */
public class DateUtil {

    public static String dateConvert(LocalDate date){
        return date.getMonthValue() + "月" + date.getDayOfMonth() + "日";
    }

    public static String yearDateConvert(LocalDate date){
        return date.getYear() + "年" + date.getMonthValue() + "月" + date.getDayOfMonth() + "日";
    }
}
