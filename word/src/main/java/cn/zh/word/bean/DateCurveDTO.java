package cn.zh.word.bean;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author zh
 */
@Data
public class DateCurveDTO {

    private LocalDate date;

    private BigDecimal value;
}
