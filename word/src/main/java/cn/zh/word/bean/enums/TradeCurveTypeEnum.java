package cn.zh.word.bean.enums;

import lombok.Getter;

/**
 * @author zh
 */
@Getter
public enum TradeCurveTypeEnum {

    /**
     * 全省最大用电负荷
     */
    MAX_ELE_LOAD(0,"全省最大用电负荷"),

    SECOND_LOAD(1, "第二产业负荷"),

    THIRD_LOAD(2, "第三产业负荷"),

    RESIDENT_LOAD(3, "城乡居民负荷")

    ;

    private final Integer code;

    private final String value;

    TradeCurveTypeEnum(Integer code, String value){
        this.code = code;
        this.value = value;
    }

    public static TradeCurveTypeEnum getByCode(Integer code){
        for (TradeCurveTypeEnum typeEnum : TradeCurveTypeEnum.values()){
            if (typeEnum.getCode().equals(code)){
                return typeEnum;
            }
        }
        throw new RuntimeException("不存在的枚举code");
    }
}
