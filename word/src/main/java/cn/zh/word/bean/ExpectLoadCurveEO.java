package cn.zh.word.bean;

import cn.zh.word.bean.handler.DateCurveJsonHandler;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author zh
 */
@Data
@TableName(value = "expect_load_curve", autoResultMap = true)
public class ExpectLoadCurveEO {

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 当天日期
     */
    private LocalDate date;

    /**
     * 曲线类型：0.全省最大用电负荷 1.第三产业降温负荷 2.城乡居民降温负荷 3.最高气温曲线
     */
    private Integer type;

    /**
     * 曲线数据
     */
    @TableField(typeHandler = DateCurveJsonHandler.class)
    private List<DateCurveDTO> curve;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}
