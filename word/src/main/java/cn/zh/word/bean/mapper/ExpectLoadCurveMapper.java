package cn.zh.word.bean.mapper;

import cn.zh.word.bean.ExpectLoadCurveEO;
import cn.zh.word.bean.TradeLoadCurveEO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * @author zh
 */
@Mapper
public interface ExpectLoadCurveMapper extends BaseMapper<ExpectLoadCurveEO> {
    List<ExpectLoadCurveEO> findByDate(LocalDate date);

    ExpectLoadCurveEO findByDateAndType(@Param("date") LocalDate date,
                                       @Param("type") Integer type);
}
