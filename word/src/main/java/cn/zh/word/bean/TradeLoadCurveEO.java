package cn.zh.word.bean;

import cn.zh.word.bean.handler.QuarterCurveJsonHandler;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author zh
 */
@Data
@TableName(value = "trade_load_curve", autoResultMap = true)
public class TradeLoadCurveEO {

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 当天日期
     */
    private LocalDate date;

    /**
     * 曲线类型：0.最大负荷曲线 1.第二产业负荷曲线 2.第三产业负荷曲线 3.居民负荷曲线
     */
    private Integer type;

    /**
     * 曲线数据
     */
    @TableField(typeHandler = QuarterCurveJsonHandler.class)
    private List<QuarterCurveDTO> curve;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;
}
