package cn.zh.word.bean.mapper;

import cn.zh.word.bean.EleLoadInfoEO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zh
 */
@Mapper
public interface EleLoadInfoMapper extends BaseMapper<EleLoadInfoEO> {
}
