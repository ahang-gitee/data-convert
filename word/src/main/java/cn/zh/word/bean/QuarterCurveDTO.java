package cn.zh.word.bean;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zh
 */
@Data
public class QuarterCurveDTO {

    private Integer quarter;

    private String time;

    private BigDecimal load;
}
