package cn.zh.word.bean.enums;

import lombok.Getter;

/**
 * @author zh
 */
@Getter
public enum ExpectLoadCurveTypeEnum {

    /**
     * 全省最大用电负荷
     */
    MAX_ELE_LOAD(0,"全省最大用电负荷"),

    THIRD_COOLING_LOAD(1, "第三产业降温负荷"),

    RESIDENT_LOAD(2, "城乡居民降温负荷"),

    MAX_TEMP(3, "最大温度"),


    ;

    private final Integer code;

    private final String value;

    ExpectLoadCurveTypeEnum(Integer code, String value){
        this.code = code;
        this.value = value;
    }

    public static TradeCurveTypeEnum getByCode(Integer code){
        for (TradeCurveTypeEnum typeEnum : TradeCurveTypeEnum.values()){
            if (typeEnum.getCode().equals(code)){
                return typeEnum;
            }
        }
        throw new RuntimeException("不存在的枚举code");
    }
}
