package cn.zh.word.bean.service;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;

/**
 * @author zh
 */
public interface ScreenDailyReportService {
    void create(LocalDate date, HttpServletResponse response) throws IOException;

    void update(LocalDate date) throws IOException;
}
