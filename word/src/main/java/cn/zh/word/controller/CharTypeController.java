package cn.zh.word.controller;

import cn.zh.word.utils.GenerateWordUtils;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.data.ChartMultiSeriesRenderData;
import com.deepoove.poi.data.SeriesRenderData;
import com.deepoove.poi.util.PoitlIOUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author zh
 */
@RestController
@Slf4j
public class CharTypeController {

    @Value("${word.templatePath}")
    private String templatePath;

    @PostConstruct
    public void test() throws IOException {
        // 读取模板文件
        File tempalteFile = ResourceUtils.getFile(templatePath);
        // 图表数据
        HashMap<String,List<? extends Number>> map = new HashMap<>();
        map.put("总用电负荷",Arrays.asList(1,2,3,4));
        map.put("第二产业",Arrays.asList(10,20,30,40));
        map.put("第三产业",Arrays.asList(15,25,35,45));
        map.put("城乡居民",Arrays.asList(100,200,300,400));

        ChartMultiSeriesRenderData charData = GenerateWordUtils.generateCharData("测试图表",
                Arrays.asList("06:00", "06:15", "06:30", "06:45"),
                map);

        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put("tradeEleLoadChar", charData);
        // 输出流
        File file = new File("classpath:report" + File.separator + "wordFile.docx");
        FileOutputStream bos = new FileOutputStream(file);

        // 生成word
        GenerateWordUtils.generateWord(tempalteFile, null, dataMap, bos);

    }

}
