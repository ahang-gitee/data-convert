package cn.zh.word.controller;

import cn.hutool.core.util.RandomUtil;
import cn.zh.word.word.DailyExecAnalyseBO;
import cn.zh.word.word.DailyExecBaseInfoBO;
import cn.zh.word.word.DailyExecInfoBO;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.plugin.table.LoopRowTableRenderPolicy;
import com.deepoove.poi.util.PoitlIOUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.TempFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author zh
 */
@RestController
@Slf4j
public class DownLoadController {

    @Value("${word.templatePath}")
    private String templatePath;

    @GetMapping("/download/resp")
    public void downloadResp(HttpServletResponse response) throws IOException {

        // execInfo 表格数据
        List<DailyExecInfoBO> execInfoList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DailyExecInfoBO execInfoBO = new DailyExecInfoBO();
            execInfoBO.setApplyHour(i + ":00-" + (i+1) + ":00" );
            execInfoBO.setRealRespLoad(RandomUtil.randomBigDecimal());
            execInfoBO.setRealRespEle(RandomUtil.randomBigDecimal());
            execInfoBO.setGapCapacity(RandomUtil.randomBigDecimal());
            BigDecimal finishRate = execInfoBO.getRealRespLoad().divide(execInfoBO.getGapCapacity(), 2 , RoundingMode.UP);
            execInfoBO.setLoadFinishRate(new BigDecimal(finishRate.toString()).setScale(2, RoundingMode.UP));
            execInfoList.add(execInfoBO);
        }

        // execAnalyse 表格数据
        List<DailyExecAnalyseBO> execAnalyseList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DailyExecAnalyseBO execAnalyseBO = new DailyExecAnalyseBO();
            execAnalyseBO.setRealRespLoad(RandomUtil.randomBigDecimal());
            execAnalyseBO.setConsNo(RandomUtil.randomString(10));
            execAnalyseBO.setConsName(RandomUtil.randomString("公司", 10));
            execAnalyseBO.setEvaluate( i % 2 == 0 ? "好" : "有待提高");
            execAnalyseBO.setRealRespLoadRate(RandomUtil.randomBigDecimal());
            execAnalyseList.add(execAnalyseBO);
        }

        // 其他字符数据
        DailyExecBaseInfoBO baseInfo = new DailyExecBaseInfoBO();
        LocalDate date = LocalDate.now();
        String dateStr = date.getYear() + " 年 " + date.getMonthValue() + " 月 " + date.getDayOfMonth() + " 日 ";
        baseInfo.setDate(dateStr);
        baseInfo.setConsNum(RandomUtil.randomInt(200));
        baseInfo.setRealRespLoad(RandomUtil.randomBigDecimal());

        // 循环表格行策略
        LoopRowTableRenderPolicy policy = new LoopRowTableRenderPolicy();

        // 绑定表格和策略
        Configure config = Configure.builder()
                .bind("execInfo", policy)
                .bind("execAnalyse", policy)
                // 对未找到标签做清空处理
                .setValidErrorHandler(new Configure.DiscardHandler())
                .build();

        // 读取模板文件
        File tempalteFile = ResourceUtils.getFile(templatePath);

        // 模板设置数据
        XWPFTemplate template = XWPFTemplate.compile(tempalteFile, config).render(
                new HashMap<String, Object>() {{
                    put("baseInfo", baseInfo);
                    put("execInfo", execInfoList);
                    put("execAnalyse", execAnalyseList);
                }}
        );

        // 写出 resp
        response.setContentType("application/octet-stream");
        String fileName = dateStr.replace(" ","") + "需求日报" + ".docx";
        response.setHeader("Content-disposition","attachment;filename="+
                new String(fileName.getBytes(StandardCharsets.ISO_8859_1)));

        // HttpServletResponse response
        OutputStream out = response.getOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(out);
        template.write(bos);
        bos.flush();
        out.flush();
        PoitlIOUtils.closeQuietlyMulti(template, bos, out);
        template.close();
    }

    /**
     * 无法读一次模板，多次写出数据
     * 加载一次模板，改为每次读模板文件，最后再关闭流
     * @param response
     * @throws IOException
     */
    @GetMapping("/download/local")
    public void downloadLocal(HttpServletResponse response) throws IOException {

        // 循环表格行策略
        LoopRowTableRenderPolicy policy = new LoopRowTableRenderPolicy();

        // 绑定表格和策略
        Configure config = Configure.builder()
                .bind("execInfo", policy)
                .bind("execAnalyse", policy)
                // 对未找到标签做清空处理
                .setValidErrorHandler(new Configure.DiscardHandler())
                .build();

        // 读取模板文件
        File tempalteFile = ResourceUtils.getFile(templatePath);

        TempFile.createTempFile("","");
        // 循环写出多个文件
        for (int j = 0; j < 3; j++) {

            XWPFTemplate template = XWPFTemplate.compile(tempalteFile, config);

            // execInfo 表格数据
            List<DailyExecInfoBO> execInfoList = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                DailyExecInfoBO execInfoBO = new DailyExecInfoBO();
                execInfoBO.setApplyHour(i + ":00-" + (i+1) + ":00" );
                execInfoBO.setRealRespLoad(RandomUtil.randomBigDecimal());
                execInfoBO.setRealRespEle(RandomUtil.randomBigDecimal());
                execInfoBO.setGapCapacity(RandomUtil.randomBigDecimal());
                BigDecimal finishRate = execInfoBO.getRealRespLoad().divide(execInfoBO.getGapCapacity(), 2 , RoundingMode.UP);
                execInfoBO.setLoadFinishRate(new BigDecimal(finishRate.toString()).setScale(2, RoundingMode.UP));
                execInfoList.add(execInfoBO);
            }

            // execAnalyse 表格数据
            List<DailyExecAnalyseBO> execAnalyseList = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                DailyExecAnalyseBO execAnalyseBO = new DailyExecAnalyseBO();
                execAnalyseBO.setRealRespLoad(RandomUtil.randomBigDecimal());
                execAnalyseBO.setConsNo(RandomUtil.randomString(10));
                execAnalyseBO.setConsName(RandomUtil.randomString("公司", 10));
                execAnalyseBO.setEvaluate( i % 2 == 0 ? "好" : "有待提高");
                execAnalyseBO.setRealRespLoadRate(RandomUtil.randomBigDecimal());
                execAnalyseList.add(execAnalyseBO);
            }

            // 其他字符数据
            DailyExecBaseInfoBO baseInfo = new DailyExecBaseInfoBO();
            LocalDate date = LocalDate.now();
            String dateStr = date.getYear() + " 年 " + date.getMonthValue() + " 月 " + date.getDayOfMonth() + " 日 ";
            baseInfo.setDate(dateStr);
            baseInfo.setConsNum(RandomUtil.randomInt(200));
            baseInfo.setRealRespLoad(RandomUtil.randomBigDecimal());

            // 模板设置数据
            template.render(
                    new HashMap<String, Object>() {{
                        put("baseInfo", baseInfo);
                        put("execInfo", execInfoList);
                        put("execAnalyse", execAnalyseList);
                    }}
            );
            template.writeToFile("C:\\Users\\17697\\Desktop\\wordTemplate\\test" + j + ".docx");

            PoitlIOUtils.closeQuietlyMulti(template);
        }


    }
}
