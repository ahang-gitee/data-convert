package cn.zh.word;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zh
 */
@SpringBootApplication
public class WordDoc {
    public static void main(String[] args) {
        SpringApplication.run(WordDoc.class, args);
    }
}