package cn.zh.word.word;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zh
 */
@Data
public class DailyExecInfoBO {

    /**
     * 需求时段（9:00-10:00）
     */
    private String applyHour;

    /**
     * 需求容量
     */
    private BigDecimal gapCapacity;

    /**
     * 响应负荷
     */
    private BigDecimal realRespLoad;

    /**
     * 响应电量
     */
    private BigDecimal realRespEle;

    /**
     * 负荷完成执行率
     */
    private BigDecimal loadFinishRate;
}
