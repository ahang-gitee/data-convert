package cn.zh.word.word;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zh
 */
@Data
public class DailyExecAnalyseBO {

    /**
     * 用户名
     */
    private String consName;

    /**
     * 户号
     */
    private String consNo;

    /**
     * 响应率
     */
    private BigDecimal realRespLoadRate;

    /**
     * 评估
     */
    private String evaluate;

    /**
     * 响应负荷
     */
    private BigDecimal realRespLoad;
}
