package cn.zh.word.word;

import lombok.Data;
import java.math.BigDecimal;

/**
 * @author zh
 */
@Data
public class DailyExecBaseInfoBO {

    /**
     * 地点名
     */
    private String placeName;

    /**
     * 日报日期（2023年5月20日）
     */
    private String date;

    /**
     * 日期范围（2023年5月20日00：00时-24:00时）
     */
    private String dateTimeRange;

    /**
     * 报告创建时间（2023年5月20日 6:00:00）
     */
    private String createTime;

    /**
     * 时段集合 (9:00-10:00、11:00-12:00、14:00-15:00、15:00-16:00)
     */
    private String applyHours;

    /**
     * 需求个数
     */
    private Integer gapNum;

    /**
     * 时段个数（9:00-10:00、11:00-12:00、14:00-15:00）
     */
    private Integer applyHourNum;

    /**
     * 用户数
     */
    private Integer consNum;

    /**
     * 供电单位数
     */
    private Integer orgNum;

    /**
     * 供电单位名集合（成都市供电公司、自贡市供电公司、雅安市供电公司、乐山市供电公司）
     */
    private String orgNames;

    /**
     * 出清（中标）用户数
     */
    private Integer bidConsNum;

    /**
     * 直接参与用户数
     */
    private Integer directJoinConsNum;

    /**
     * 代理用户数
     */
    private Integer proxyConsNum;

    /**
     * 异常用户数
     */
    private Integer errorConsNum;

    /**
     * 负荷执行率
     */
    private BigDecimal loadFinishRate;

    /**
     * 当日所有时段中最大的需求容量
     */
    private BigDecimal gapCapacityMax;

    /**
     * 响应电量
     */
    private BigDecimal realRespLoad;

    /**
     * 错误采集条数
     */
    private Integer errorCollectNum;

    /**
     * 错误计量点数据
     */
    private Integer errorPointNum;

}
