package cn.zh.word.word;

import lombok.Data;

/**
 * @author zh
 */
@Data
public class DailyErrorCountBO {

    /**
     * 用户名
     */
    private String consName;

    /**
     * 异常计量点个数
     */
    private Integer errorPointNum;
}
